<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SocialStreamGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_stream_games', function (Blueprint $table) {
            $table->increments('id');
            $table->string('game_id');
            $table->string('game_name');
            $table->string('service');
            $table->timestamps();
    
            $table->index(['service', 'game_name']);
            $table->index(['game_id']);
    
            $table->unique(['service', 'game_id']);
            
            // todo: ichepster: добавить foreign keys или нет.. таблицы скорее всего будут лежать в пределах одной схемы.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_stream_games');
    }
}
