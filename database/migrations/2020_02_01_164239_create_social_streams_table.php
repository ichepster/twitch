<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialStreamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_streams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('service');
            $table->string('game_id');
            $table->string('stream_id'); // возможно стоит переделать на int. Честно говоря не владею ситуацию какие id могут быть у стриминговых площадок.
            $table->integer('viewer_count');
            $table->timestamps();
            
            // todo: ichepster: пока без ключей, т.к. неизвестно какие запросы на чтение будут применяться к данной таблице.
            // todo: ichepster: добавить foreign keys или нет.. таблицы скорее всего будут лежать в пределах одной схемы.
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_streams');
    }
    
}
