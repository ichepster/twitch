@extends('layouts.app')

@section('content')
    <div class="top-left">
        <h1>Добавить игру</h1>
    </div>

    <div class="top-right">
        <a class="btn btn-abort" href="{{ route('games.index') }}">отмена</a>
    </div>


    <form id="games-add-form" action="{{ route('games.store') }}" method="POST">
        @csrf

        <div class="control-group">
            <div class="control-label">
                <label for="form[service]">Сервис</label>
            </div>
            <div class="control-input">
                <select class="control-select" name="form[service]">
                    <option value>Выберите сервис</option>

                    @foreach ($services as $service)
                        <option value="{{$service}}" @if ($service == old('form.service')) selected @endif >{{@ucfirst($service)}}</option>
                    @endforeach

                </select>
            </div>
        </div>

        <p>Укажите название или ID игры:</p>

        <div class="control-group">
            <div class="control-input">
                <input class="control-input" name="form[game_name]" type="text" value="{{ old('form.game_name') }}" placeholder="Название игры"/>
            </div>
        </div>


        <div class="control-group">
            <div class="control-input">
                <input class="control-select" name="form[game_id]" type="text" value="{{ old('form.game_id') }}" placeholder="ID игры"/>
            </div>
        </div>

        <div class="control-group">
            <button class="btn btn-success" type="submit">Добавить</button>
        </div>

    </form>

    <!-- errors -->


    @foreach ($errors->all() as $error)
        <p class="error-message">{{$error}}</p>
    @endforeach



    <script>
        (() => {
            document.querySelector('#games-add-form').addEventListener('submit', e => {
                let
                    $this = e.target,
                    name = $this.querySelector('input[name="form[game][name]"]'),
                    id = $this.querySelector('input[name="form[game][id]"]'),
                    service = $this.querySelector('select[name="form[service]"]'),
                    errClass = 'error',
                    callback = i => i.classList.contains(errClass);

                // [name, id, service].forEach(i => i.value
                //     ? i.classList.remove(errClass)
                //     : i.classList.add(errClass));

                if (callback(service) || (callback(name) && callback(id))) {
                    e.preventDefault();
                }
            });
        })()
    </script>
{{--}}
    <script>
        (() => {
            let

                service   = document.querySelector('.control-group select[name="form[service]"]'),
                search    = document.querySelector('.control-group input[name="form[search]"]'),
                searchBtn = document.querySelector('.control-group button[data-action="search"]'),
                game      = document.querySelector('.control-group select[name="form[game_id]"]');


            searchBtn.addEventListener('click', function () {
                let body = encodeURIComponent([
                    ['service', service.value],
                    ['search', search.value],
                ].map(i => i.join('=')).join('&'));

                fetch(@json(route('games.search')), {
                    method: 'post',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-CSRF-TOKEN': document.querySelector('input[name="_token"]').value
                    },
                    body: JSON.stringify({
                        service: service.value,
                        search: search.value,
                    })
                }).then(r => r.json()).then(i => console.log(i));
            });

            // disable inputs.
            // search.prop('disabled', true);
            // game.prop('disabled', true);

            // console.log(service, search, game);
            //
            // // search.disabled    = true;
            // // searchBtn.disabled = true;
            // // game.disabled      = true;
            // //
            // service.addEventListener('change', function () {
            //     search.disabled = !this.value;
            //
            //     console.log('service:', !this.value);
            // });
            //
            // search.addEventListener('keyup', function () {
            //     console.log('value:', this.value);
            //
            //
            // });
        })();

    </script>

    {{--}}
@endsection