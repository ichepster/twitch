@extends('layouts.app')

@section('content')

    <div class="top-left">
        <h1>Управление играми</h1>
    </div>

    <div class="top-right">
        <a class="btn btn-success" href="{{ route('games.create') }}">Добавить</a>
    </div>

    <div>
        <table class="table" width="100%" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th>ID</th>
                <th>Сервис</th>
                <th>Название игры</th>
                <th>ID игры</th>
                <th>Управление</th>
            </tr>
            </thead>

            <tbody>
            @forelse($records as $record)
                <tr>
                    <th>{{$record->id}}</th>
                    <th>{{$record->service}}</th>
                    <th>{{$record->game_name}}</th>
                    <th>{{$record->game_id}}</th>

                    <th class="control-panel">
                        {{--<button>Статистика</button>--}}

                        <form action="{{route('games.destroy', $record)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button>Удалить</button>
                        </form>

                    </th>
                </tr>
            @empty
                <tr><td colspan="99" align="center">Список пуст</td></tr>
            @endforelse
            </tbody>

        </table>
    </div>

@endsection