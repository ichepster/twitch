<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>

        * {
            box-sizing: border-box;
        }

        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        /*.top-left {*/
            /*position: absolute;*/
            /*left: 10px;*/
            /*top: 10px;*/
        /*}*/

        /*.top-right {*/
            /*position: absolute;*/
            /*right: 10px;*/
            /*top: 18px;*/
        /*}*/

        .content {
            /*text-align: center;*/
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        h1 {
            line-height: 1.5em;
            margin: 0;
        }

        table.table tr {
            border: 1px solid gray;
            border-collapse: collapse;
        }

        table.table td {
            border-top: 1px solid gray;
            padding: 5px;
        }

        table.table th {
            font-size: 1.2em;
            font-weight: bold;
        }

        .page { padding: 80px 25px 0; }

        .btn {
            display: inline-block;
            border: 1px solid black;
            border-radius: 8px;
            padding: 10px 15px;
            text-align: center;
            text-decoration: none;
            cursor: pointer;
        }

        .btn-success {
            background: seagreen;
            color: white;
        }

        .btn-abort {
            background: firebrick;
            color: white;
        }

        .control-group {
            margin-bottom: 5px;
        }

        .control-group div.control-label {
            display: inline-block;
            width: 250px;
            margin-right: 25px;
        }

        .control-group div.control-input {
            display: inline-block;
            min-width: 400px;
        }

        .control-group select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 4px 12px;
            width: auto;
        }

        .control-group input {
            border: 1px solid gray;
            border-radius: 4px;
            padding: 4px;
        }

        .control-group input, .control-group select {
            height: 35px;
            min-width: 300px;
        }

        .control-group input.error, .control-group select.error {
            border-color: red;
        }

        p.error-message { color: red; }

        .table thead th {
            border-bottom: 1px solid gray;
        }

        .table .control-panel form { display: inline-block; }

        main { position: relative; }

    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
