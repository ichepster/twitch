<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'games', 'as' => 'games.',], function () {
    Route::get ('',       ['as' => 'index',  'uses' => 'GameController@index',]);
    Route::get ('create', ['as' => 'create', 'uses' => 'GameController@create',]);
    Route::post('store',  ['as' => 'store',  'uses' => 'GameController@store']);
    
    Route::group(['prefix' => '{id}',], function () {
        Route::delete('',         ['as' => 'destroy', 'uses' => 'GameController@destroy']);
//        Route::get   ('',         ['as' => 'update',  'uses' => 'GameController@update']);
//        Route::get   ('/details', ['as' => 'details', 'uses' => 'GameController@details']);
    });
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
