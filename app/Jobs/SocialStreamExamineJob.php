<?php

namespace App\Jobs;

use App\SocialStreamGame;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Repository\SocialStreamRepositoryInterface;

class SocialStreamExamineJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $streamRepository;
    
    /**
     * SocialStreamExamineJob constructor.
     *
     * @param \App\Repository\SocialStreamRepositoryInterface $repository
     */
    public function __construct(SocialStreamRepositoryInterface $repository)
    {
        $this->streamRepository = $repository;
    }
    
    /**
     * Execute the job.
     *
     * @param SocialStreamGame $model
     *
     * @return void
     */
    public function handle(SocialStreamGame $model)
    {
        foreach ($model->all() as $item) {
            /** @var \App\Contracts\SocialStream $service */
            $service = app('socialStreamService/' . $item->getAttribute('service'));

            $service->callForStreamByGame($item->getAttribute('game_id'), function ($item, $service) {
                // todo: ichepster: нужен метод в результате для проверки - "живой" ли стрим.
                /** @var \App\Contracts\SocialStream $service */
                if ($item->type != 'live') {
                    return;
                }

                $this->streamRepository->create([
                    'stream_id'    => $item->id,
                    'game_id'      => $item->game_id,
                    'service'      => $service->getServiceName(),
                    'viewer_count' => $item->viewer_count,
                ]);
            });
        }

        // todo: ichepster: так же имеет смысл передать сюда класс-логер, чтобы записывать логи или отправлять результат работы по почте или другим способом
    }
    
}
