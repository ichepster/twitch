<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Library\ichepster\SocialStreamService;

class SocialStreamServiceExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function passes($attribute, $value)
    {
        return in_array($value, app('socialStreamService')::avail());
    }
    
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Service doesn't exists or invalid service.";
    }
    
}
