<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\SocialStreamGame;


class SocialStreamUniqueRule implements Rule
{
    
    protected $game_name;
    protected $game_id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($game_id = null, $game_name = null)
    {
        $this->game_id   = $game_id;
        $this->game_name = $game_name;
    }
    
    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $field = $this->game_id ? 'game_id' : 'game_name';
    
        return SocialStreamGame::where('service', $value)
                ->where($field, $this->$field)
                ->count() == 0;
    }
    
    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Game already added to list.';
    }
    
}
