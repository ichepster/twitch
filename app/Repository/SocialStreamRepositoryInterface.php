<?php

namespace App\Repository;

interface SocialStreamRepositoryInterface
{
    
    public function create(array $attributes);
    
    public function find($id);
    
    public function findWith(array $filter);
    
}