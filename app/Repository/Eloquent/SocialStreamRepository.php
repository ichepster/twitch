<?php

namespace App\Repository\Eloquent;

use Illuminate\Database\Eloquent\Model;
use App\Repository\SocialStreamRepositoryInterface;
use Illuminate\Support\Arr;

class SocialStreamRepository implements SocialStreamRepositoryInterface
{
    
    /** @var \Illuminate\Database\Eloquent\Model  */
    protected $model;
    
    public function __construct(Model $model)
    {
        $this->model = $model;
    }
    
    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }
    
    public function find($id)
    {
        return $this->model->find($id);
    }
    
    public function findWith(array $filter = [])
    {
        $query = $this->model->newQuery();
        
        if (!empty($filter)) {
            if (Arr::has($filter, 'game_id') && $filter['game_id']) {
                if (is_array($filter['game_id'])) {
                    $query->whereIn('game_id', $filter['game_id']);
                } else {
                    $query->where('game_id', $filter['game_id']);
                }
            }
            
            if (Arr::has($filter, 'date_start') && $filter['date_start']) {
                $query->where('created_at', '>=', new \DateTime($filter['date_start']));
            }
            
            if (Arr::has($filter, 'date_end') && $filter['date_end']) {
                $query->where('created_at', '<=', new \DateTime($filter['date_end']));
            }
        }
        
        return $query->get();
    }
    
}
