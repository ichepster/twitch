<?php

namespace App\Providers;

use App\SocialStream;
use Illuminate\Support\ServiceProvider;

use App\Repository;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Repository\SocialStreamRepositoryInterface::class, function () {
            return new Repository\Eloquent\SocialStreamRepository(new SocialStream());
        });
    }
    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
