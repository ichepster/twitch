<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Library\ichepster\SocialStreamService;

class SocialStreamProvider extends ServiceProvider
{
    
    /** @var string */
    protected $bindNamespace = 'socialStreamService';
    
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind($this->bindNamespace, function () {
            return new SocialStreamService();
        });
        
        $this->app->bind($this->bindNamespace . '/twitch', function () {
            return new SocialStreamService\Twitch();
        });
    }
    
    /**
     * Bootstrap services.
     *
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @return void
     */
    public function boot()
    {
        $service = $this->app->make($this->bindNamespace);
        $search  = $this->bindNamespace . '/';
        
        foreach ($this->app->getBindings() as $serviceName => $null) {
            if (strpos($serviceName, $search) === 0) {
                $service->push(str_replace($search, '', $serviceName));
            }
        }
    }
    
}
