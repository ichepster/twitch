<?php

namespace App\Library\ichepster;

class SocialStreamService
{
    
    protected static $avail = [];
    
    public function push($serviceName)
    {
        array_push(static::$avail, $serviceName);
    }
    
    public static function avail()
    {
        return static::$avail;
    }
    
}