<?php

namespace App\Library\ichepster\SocialStreamService;

use App\Contracts\SocialStream;

class Twitch implements SocialStream
{
    
    /**
     * @var \romanzipp\Twitch\Twitch
     */
    protected $service;
    
    // todo: ichepster: надо перенести либо в app/$packageName.config, либо в БД
    /** @var string API-ключ, необходим для авторизации в Twitch-API */
    protected $clientId = '1bpllzjn8j0ldgroa3v06ha0iztx7c';
    
    /** @var string Название сервиса */
    protected $serviceName = 'twitch';
    
    
    public function __construct()
    {
        $this->service = app(\romanzipp\Twitch\Twitch::class);
        $this->service->setClientId($this->clientId);
    }
    
    public function searchGameById($id)
    {
        return $this->shift($this->service->getGameById($id));
    }
    
    public function searchGameByName($name)
    {
        return $this->shift($this->service->getGameByName($name));
    }
    
    public function callForStreamByGame(int $id, callable $func)
    {
        do {
            // max 100 items by request [api documentation]
            $result = $this->service->getStreamsByGame($id, ['first' => 100], isset($result) ? $result->next() : null);
        
            foreach ($result->data as $item) {
                call_user_func($func, $item, $this);
                
//                // todo: ichepster: for testing; drop it.
//                break 2;
            }
            
//            // need wait - for not confirmed tokens;
//            sleep(1);
        
        } while ($result->count() > 0);
    }
    
    public function getServiceName()
    {
        return $this->serviceName;
    }
    
    protected function shift($result)
    {
        if ($result->success()) return $result->shift();
        return null;
    }
    
}
