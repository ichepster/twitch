<?php

namespace App\Contracts;

interface SocialStream
{
    
    /**
     * Найти игру в API по названию
     * @param $name
     *
     * @return mixed
     */
    public function searchGameByName($name);
    
    /**
     * Найти игру в API по ID
     * @param $id
     *
     * @return mixed
     */
    public function searchGameById($id);
    
    /**
     * Извлекается страница данных и применяется callback для обработки, после
     * чего процесс циклично повторяется до тех пор пока не будут прочитаны все
     * страницы.
     *
     * @param int      $id
     * @param callable $func
     *
     * @return mixed
     */
    public function callForStreamByGame(int $id, callable $func);
    
}