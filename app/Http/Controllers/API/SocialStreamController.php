<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\SocialStreamRequest;
use App\Repository\SocialStreamRepositoryInterface;
use App\SocialStream;
use Illuminate\Http\Request;

class SocialStreamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SocialStreamRepositoryInterface $repository, SocialStreamRequest $request)
    {
        // todo: add pagination
        $data = $repository->findWith($request->get('filter', []))->map(function ($item) {
            return [
                'stream_id'    => $item->stream_id,
                'game_id'      => $item->game_id,
                'viewer_count' => $item->viewer_count,
                'date'         => $item->created_at,
            ];
        });
        
        return response()->json(['data' => $data]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response(null, 501);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\SocialStream $socialStream
     *
     * @return \Illuminate\Http\Response
     */
    public function show(SocialStream $socialStream)
    {
        return response(null, 501);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\SocialStream        $socialStream
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialStream $socialStream)
    {
        return response(null, 501);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SocialStream $socialStream
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialStream $socialStream)
    {
        return response(null, 501);
    }
    
}
