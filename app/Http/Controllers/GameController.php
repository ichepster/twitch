<?php

namespace App\Http\Controllers;

use App\Rules\SocialStreamUniqueRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Rules\SocialStreamServiceExist;
use App\SocialStreamGame;

class GameController extends Controller
{
    
    public function index(SocialStreamGame $model)
    {
        return view('games.index', [
            'records'  => $model->all(),
        ]);
    }
    
    public function create()
    {
        return view('games.create', [
            'services' => app('socialStreamService')::avail(),
        ]);
    }
    
    public function store(Request $request, SocialStreamGame $model)
    {
        // custom validate
        $validator = Validator::make($request->all(), [
            'form.game_id'    => ['required_without:form.game_name'],
            'form.game_name'  => ['required_without:form.game_id',],
            'form.service'    => [
                'required',
                new SocialStreamServiceExist(),
                new SocialStreamUniqueRule($request->form['game_id'], $request->form['game_name']),
            ],
        ]);
    
        if ($validator->fails()) {
            return redirect(route('games.create'))
                ->withErrors($validator)
                ->withInput();
        }
    
        // search game
        /** @var \App\Contracts\SocialStream $service */
        $service = app('socialStreamService/' . $request->form['service']);
        
        $game = $request->form['game_id']
            ? $service->searchGameById($request->form['game_id'])
            : $service->searchGameByName($request->form['game_name']);
        
        if (!$game) {
            return redirect(route('games.create'))
                ->withErrors([
                    'Searching game not found',
                ])
                ->withInput();
        }
        
        // store data
        $model->create([
            'service'   => $request->form['service'],
            'game_id'   => $game->id,
            'game_name' => $game->name,
        ]);
        
        return redirect(route('games.index'));
    }
    
    public function destroy($id)
    {
        SocialStreamGame::destroy($id);
        
        return redirect(route('games.index'));
    }
    
}
