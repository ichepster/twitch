<?php

namespace App\Http\Middleware;

use Closure;
use IPTools;

class WhiteListIPMiddleware
{
    
    /**
     * Handle an incoming request.
     *
     * @param          $request
     * @param \Closure $next
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        
        $whiteList = [
//            '127.0.0.1',
//            '127.0.0.2-127.0.0.3',
            '127.0.0.*',
        ];
    
        $isAllowed = false;
        $clientIp = new IPTools\IP($request->ip());
        
        foreach ($whiteList as $ip) {
            if ($isAllowed = \IPTools\Range::parse($ip)->contains($clientIp)) break;
        }
        
        // todo: ichepster: этот вариант подходит для routes/web.php, но для api, наверное, нужно вернуть запрещающий статус. Пока не знаю как это сделать.
        if (!$isAllowed) {
            return redirect('home');
        }
    
        return $next($request);
    }
    
}
