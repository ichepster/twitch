<?php

namespace App\Http\Requests;

use App\Rules\SocialStreamServiceExist;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SocialStreamSearch extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search'  => ['required'],
            'service' => ['required', new SocialStreamServiceExist],
        ];
    }
    
    public function messages()
    {
        return [
            'service.required' => 'Empty parameter: service',
            'search.required'  => 'Empty parameter: search',
        ];
    }
    
    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors()->all(), 422));
    }
}
