<?php

namespace App\Http\Requests;

use App\Rules\SocialStreamServiceExist;
use Illuminate\Foundation\Http\FormRequest;

class SocialServiceGameCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'form.game.id'    => ['required_without:form.game.name'],
            'form.game.name'  => ['required_without:form.game.id'],
            'form.service'    => ['required', new SocialStreamServiceExist()],
        ];
    }
    
}
